<%--
  Created by IntelliJ IDEA.
  User: hanyuan
  Date: 2018/5/27
  Time: 16:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>servlet async support</title>
</head>
<body>
<div id="content">

</div>
<h1 id="text"></h1>
<script src="../assets/js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script>
    deferred();
    function deferred() {
        $.get('defer',function (data) {
            $("#content").append("<h1>"+data+"</h1>");
            deferred();
        })
    }
</script>
</body>
</html>
