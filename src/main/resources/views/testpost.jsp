<%--
  Created by IntelliJ IDEA.
  User: han.yuan
  Date: 2018/5/28
  Time: 21:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>测试post提交数据</title>
</head>
<body>
<div id="content2">

</div>
<button onclick="testPost()">发起一个post请求</button>
<script src="../assets/js/jquery-3.3.1.min.js" type="text/javascript"></script>
<script>
    function testPost() {
        $.ajax({
            url:"/testPost",
            type:"POST",
            data:JSON.stringify({id:111,name:"你大爷还是你大爷"}),
            contentType:"application/json;charset=utf-8",
//            dataType:"json",
            success: function(data){
                console.log(data);
                $("#content2").append("<h1>"+JSON.stringify(data)+"</h1>");
            }
        })
    }
</script>
</body>
</html>
