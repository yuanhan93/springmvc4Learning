package com.just.springmvc4.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class HelloController {
    @RequestMapping(value={"","/index"})
    public String hello(){
        return "index";
    }
    @RequestMapping(value="/testPost",method = RequestMethod.POST)
    @ResponseBody
    public Object testPost(){
        Map<String,String> map=new HashMap<>();
        map.put("result","ok");
        map.put("success","true");
        return map;
    }

}
