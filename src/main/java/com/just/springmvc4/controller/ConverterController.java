package com.just.springmvc4.controller;

import com.just.springmvc4.domain.DemoObj;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConverterController {
    @RequestMapping(value = "/converter",produces = "application/x-wisely")
    public DemoObj converter(@RequestBody DemoObj demoObj){
         return demoObj;
    }
}
