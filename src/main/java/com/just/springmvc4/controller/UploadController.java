package com.just.springmvc4.controller;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class UploadController {
    @RequestMapping(value = "/upload",method = RequestMethod.POST)
    public String uploadFile(MultipartFile file){
        try {
            FileUtils.writeByteArrayToFile(new File("D:\\file\\"+file.getOriginalFilename()),file.getBytes());
            return "good work";
        }catch (Exception e){
            e.printStackTrace();
            return "wrong";
        }
    }
    @RequestMapping(value = "/uploadMulti",method = RequestMethod.POST)
    public String uploadMultiFile(@RequestParam("files") MultipartFile[] files){
      try {
          for(MultipartFile file:files){
              FileUtils.writeByteArrayToFile(new File("D:\\file\\"+file.getOriginalFilename()),file.getBytes());
          }
          return "all file is well.";
      }catch (Exception e){
          e.printStackTrace();
          return "I'm sorry to tell you that:"+e.getMessage();
      }
    }
}
