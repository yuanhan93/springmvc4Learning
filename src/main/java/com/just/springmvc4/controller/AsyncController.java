package com.just.springmvc4.controller;

import com.just.springmvc4.service.PushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
public class AsyncController {
    @Autowired
    private PushService pushService;

    @RequestMapping(value = "/defer",produces = "text/html;charset=UTF-8")
    public DeferredResult<String> deferredCall(){
        return pushService.getAsyncUpdate();
    }

}
