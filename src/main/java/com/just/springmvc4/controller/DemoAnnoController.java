package com.just.springmvc4.controller;

import com.just.springmvc4.domain.DemoObj;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * RequestMapping注解有六个属性，下面我们把她分成三类进行说明。
 *
 【1、 value， method；】
 value：指定请求的实际地址，指定的地址可以是URI Template 模式；
 method： 指定请求的method类型， GET、POST、PUT、DELETE等；

 【2、consumes，produces；】
 consumes： 指定处理请求的提交内容类型（Content-Type），例如application/json, text/html;
 produces: 指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回；

 【3、 params，headers；】
 params： 指定request中必须包含某些参数值时，才让该方法处理。
 headers： 指定request中必须包含某些指定的header值，才能让该方法处理请求。
 */
@Controller
@RequestMapping("/anno")
public class DemoAnnoController {
    @RequestMapping(produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String index(HttpServletRequest request){
        return "url:"+request.getRequestURI()+"can access";
    }
    @RequestMapping(value = "/pathvar/{str}",produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String DemoPathVar(@PathVariable String str,HttpServletRequest request){
        return "url:"+request.getRequestURI()+" can access,str:"+str;
    }
    @RequestMapping(value = "/requestParam",produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String passRequestParam(Long id,HttpServletRequest request){
        return "url:"+request.getRequestURI()+" can access,id:"+id;
    }
    @RequestMapping(value = "/obj",produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String passObj(DemoObj demoObj, HttpServletRequest request){
        return "url:"+request.getRequestURI()+" can access,objId："+demoObj.getId()+",objName："+demoObj.getName();
    }

    @RequestMapping(value = {"/name1","/name2"},produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String remove(HttpServletRequest request){
        return "url:"+request.getRequestURI()+" can access";
    }

}
