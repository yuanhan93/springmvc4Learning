package com.just.springmvc4.controller;

import java.util.Random;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务端推送
 * 在消息体后面有两个换行符\n，代表当前消息体发送完毕，一个换行符标识当前消息并未结束，
 * 浏览器需要等待后面数据的到来后再触发事件；
 */
@RestController
public class SSEController {
    @RequestMapping(value ="/push",produces = "text/event-stream;charset=UTF-8")
    public String sse(){
        Random r=new Random();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "data:当前增加的收入："+r.nextInt(1000)+"元"+"\n\n";
    }
}
